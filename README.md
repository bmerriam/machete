# macha

macha will show you random updates, stats, alerts.. whatever you'd like it to tell you.

## Backend
The backend reads data feeds in multiple different formats, parses or transcodes, then stores in a common format. Data can be text or binary, stored locally or accessible via a reference.

![UML](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuR8AIarCgRJcilB9B2vH2CWfJS_CAxRcAYx8oIpXKh0ApqZCINKCyBG1ihs2_Vti838AYUc9cNaW-TJ4aepK8f1qXAyFkgnpOebROd99CKWwn3KY15tUCIUr22WBDpirBqK1OhDLIf8rbmEG3tGZ0000)
* [Diagram src](http://www.plantuml.com/plantuml/uml/SoWkIImgAStDuR8AIarCgRJcilB9B2vH2CWfJS_CAxRcAYx8oIpXKh0ApqZCINKCyBG1ihs2_Vti838AYUc9cNaW-TJ4aepK8f1qXAyFkgnpOebROd99CKWwn3KY15tUCIUr22WBDpirBqK1OhDLIf8rbmEG3tGZ0000)

The backend uses plugins to parse the data into the database. There are several examples currently the repo, but here is a simple example:

```python
"""
Cat facts!

Sources:
* http://www.cataas.com

DataMap:
* link: cat.jpg
* text: Cat!

"""
import requests
import json
import logging

class CatFacts:
    def __init__(self):
        self.logger = logging.getLogger("macha.plugins.catfacts")
        self.endpoint = "https://cat-fact.herokuapp.com/facts/random"

    def fetch(self):
        r = requests.get(self.endpoint)
        return r.json()

    def parse(self, raw_data):
        return raw_data["text"]

    def run(self):
        raw_data = self.fetch()
        parsed_data = self.parse(raw_data)
        return parsed_data
```

## Frontend
The frontend access the API to read the list of items stored in the database.  This is a simple flat, SPA.

![PNG](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuR8A1h6ikQo2KXDJ51m3F85CdypY4WNFajJSOcikXzIy5A0w0000)
* [Diagram src](http://www.plantuml.com/plantuml/uml/SoWkIImgAStDuR8A1h6ikQo2KXDJ51m3F85CdypY4WNFajJSOcikXzIy5A0w0000)

## API
The api consists of a [FastAPI](https://fastapi.tiangolo.com/) implementation using [CRUDRouter](https://github.com/awtkns/fastapi-crudrouter).  This allows for simplified schema to provide a CRUD routing from the database.

![PNG](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuR9AJ2x9BxBcAYx8oIpXKh1oJorHSCp9iOC8A2Ic9sRcGONzyrBHXPsBKXDBKa4oDcWoeGKfE6HSKdDI8J8rM6L0rjTmfUQbA43SP5MKMboQKsBDvG0K0peP0000)
* [Diagram src](http://www.plantuml.com/plantuml/uml/SoWkIImgAStDuR9AJ2x9BxBcAYx8oIpXKh1oJorHSCp9iOC8A2Ic9sRcGONzyrBHXPsBKXDBKa4oDcWoeGKfE6HSKdDI8J8rM6L0rjTmfUQbA43SP5MKMboQKsBDvG0K0peP0000)
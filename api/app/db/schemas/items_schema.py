from datetime import date
from pydantic import BaseModel


class Items(BaseModel):
    headline: str
    detail: str
    url: str
    event_type: str
    date: date

    class Conifg:
        orm_mode = True

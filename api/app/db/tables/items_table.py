import sqlalchemy

from db.config import metadata, engine

items = sqlalchemy.Table(
    "items",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("headline", sqlalchemy.String),
    sqlalchemy.Column("detail", sqlalchemy.String),
    sqlalchemy.Column("url", sqlalchemy.String),
    sqlalchemy.Column("event_type", sqlalchemy.String),
    sqlalchemy.Column("date", sqlalchemy.Date),
)

metadata.create_all(bind=engine)

import importlib
import logging


class Plugins:
    def __init__(self):
        self.logger = logging.getLogger("macha.plugins")
        self.loaded_plugins = []

    def load_plugin(self, plugin):
        """Load a plugin for macha

        Load a specific plugin.

        :params plugin: - A string naming a class to load
        """
        if plugin is None or plugin == "":
            self.logger.error("Plugin string cannot be empty")

        try:
            (module, x, classname) = plugin.rpartition(".")
            if module == "":
                raise Exception("Unable to find module name")
            mod = importlib.import_module(module)
            klass = getattr(mod, classname)
            self.loaded_plugins.append(klass())
        except Exception as e:
            self.logger.error(f"Could not load plugin {plugin}: {str(e)}")

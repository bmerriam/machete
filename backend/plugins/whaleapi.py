"""
WhaleAPI using USGS data


Sources:
* https://www.gbif.org/developer/species
* https://www.gbif.org/developer/occurrence

DataMap:
* link: None
* text: Whale! - <Thumbnail of google map>

"""
import requests
import json
import logging
from random import randrange
from datetime import datetime


class Whales:
    def __init__(self):
        self.logger = logging.getLogger("macha.plugins.whaleapi")
        self.endpoint = "https://api.gbif.org/v1/occurrence/search?q=whale"

    def fetch(self):
        r = requests.get(self.endpoint)
        return r.json()

    def parse(self, raw_data):
        whale = None
        text = None
        while whale == None:

            fish = raw_data["results"][randrange(0, len(raw_data["results"]))]
            if (
                "decimalLongitude" in fish
                and "decimalLatitude" in fish
                and "genericName" in fish
            ):
                text = f"{fish['genericName']} - {fish['country']}"
                whale = fish
        return {
            "headline": fish["genericName"],
            "detail": fish["country"],
            "url": "",
            "event_type": "info",
            "date": datetime.today().strftime("%Y-%m-%d"),
        }

    def run(self):
        raw_data = self.fetch()
        parsed_data = self.parse(raw_data)
        return parsed_data

"""Random Weather

Returns:
    string: City Name and current weather
"""

import requests
import json
import logging
from random import randrange
import random
from datetime import datetime

# from pygeocoder import Geocoder, GeocoderError
from random import uniform
from geopy.geocoders import Nominatim
from os import environ, path
from dotenv import load_dotenv

# Load configuration values from the .env file
basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

OPENWEATHER_API = environ.get("OPENWEATHER_API")


class Weather:
    def __init__(self):
        self.logger = logging.getLogger("macha.plugins.weather")
        self.endpoint = "https://api.openweathermap.org/data/2.5/onecall?"

    def get_location(self):

        locator = Nominatim(user_agent="myGeocoder")

        location = None
        while location == None:
            lon, lat = round(uniform(-180, 180), 2), round(uniform(-90, 90), 2)

            coordinates = f"{lat}, {lon}"
            location = locator.reverse(coordinates)

        return lon, lat, location

    def fetch(self, lon, lat):
        """Get the actual weather

        Returns:
            string: weather
        """
        try:
            r = requests.get(
                f"{self.endpoint}lat={lat}&lon={lon}&units=imperial&appid={OPENWEATHER_API}"
            )

        except Exception as e:
            self.logger.error(f"Unable to get weather: {e}")
            raise Exception(f"Unable to get weather: {e}")

        return r.json()

    def parse(self, raw_data, location, lon, lat):
        weather_line = f"{raw_data['current']['weather'][0]['main']} and {(raw_data['current']['temp'] * 9/5) + 32}"
        location_line = f"https://openweathermap.org/weathermap?basemap=map&cities=true&layer=temperature&lat={lat}8&lon={lon}&zoom=10"
        return {
            "headline": str(location),
            "detail": weather_line,
            "url": location_line,
            "event_type": "info",
            "date": datetime.today().strftime("%Y-%m-%d"),
        }

    def run(self):
        # get a random location
        lon, lat, location = self.get_location()

        # Get the weather data
        weather_data = self.fetch(lon, lat)

        # Clean up weather data
        parsed_data = self.parse(weather_data, location, lon, lat)

        return parsed_data

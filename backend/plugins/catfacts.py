"""
Cat facts!

Sources:
* http://www.cataas.com

DataMap:
* link: cat.jpg
* text: Cat!

"""
import requests
import json
import logging
from datetime import datetime


class CatFacts:
    def __init__(self):
        self.logger = logging.getLogger("macha.plugins.catfacts")
        self.endpoint = "https://cat-fact.herokuapp.com/facts/random"

    def fetch(self):
        r = requests.get(self.endpoint)
        return r.json()

    def parse(self, raw_data):
        return {
            "headline": "CatFact",
            "detail": raw_data["text"],
            "url": "https://cataas.com/cat/gif",
            "event_type": "info",
            "date": datetime.today().strftime("%Y-%m-%d"),
        }

    def run(self):
        raw_data = self.fetch()
        parsed_data = self.parse(raw_data)
        return parsed_data

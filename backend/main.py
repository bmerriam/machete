#!/usr/bin/python3
"""
 Backend - Parses data sources into a common format to store in a DB
"""

import asyncio
import logging
import time
import requests
import schedule
import plugin_manager
import sys

API_ENDPOINT = "http://api:5050/api/items"

# Setup logging
logger = logging.getLogger("macha")
logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)


def main():

    # Lets start our plugin
    pm = plugin_manager.Plugins()

    # Specify the plugins to load
    pm.load_plugin("plugins.whaleapi.Whales")
    pm.load_plugin("plugins.catfacts.CatFacts")
    pm.load_plugin("plugins.weather.Weather")

    # Create the schedule to run task every 10 minutes.
    schedule.every(10).minutes.do(run, pm=pm)

    # wait forever..
    while True:
        schedule.run_pending()
        time.sleep(1)


def run(pm):
    for loaded_plugin in pm.loaded_plugins:
        logger.info(f"Running Module: {loaded_plugin.__module__}")

        plugin_data = loaded_plugin.run()
        logger.debug(plugin_data)

        try:
            requests.post(API_ENDPOINT, json=plugin_data)
        except Exception as e:
            logger.error(
                f"Unable to publish data for {loaded_plugin} : {plugin_data} : {e}"
            )


if __name__ == "__main__":
    # start those nested async tasks
    logger.info("Running startup")
    asyncio.run(main())
